﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {
        OperationService _operationService = null;
        IServiceProvider _serviceProvider = null;
        IHttpContextAccessor _httpContextAccessor = null;
        public HomeController(OperationService operationService, IServiceProvider serviceProvider, IHttpContextAccessor httpContextAccessor)
        {
            _operationService = operationService;
            _serviceProvider = serviceProvider;
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index()
        {
            var transient1 = (IOperationTransient)_serviceProvider.GetService(typeof(IOperationTransient));
            var scoped1 = (IOperationScoped)_serviceProvider.GetService(typeof(IOperationScoped));
            var singleton1 = (IOperationSingleton)_serviceProvider.GetService(typeof(IOperationSingleton));
            var singletoninstance1 = (IOperationSingletonInstance)_serviceProvider.GetService(typeof(IOperationSingletonInstance));

            var serviceProvider = _httpContextAccessor.HttpContext.RequestServices;

            var transient2 = (IOperationTransient)serviceProvider.GetService(typeof(IOperationTransient));
            var scoped2 = (IOperationScoped)serviceProvider.GetService(typeof(IOperationScoped));
            var singleton2 = (IOperationSingleton)serviceProvider.GetService(typeof(IOperationSingleton));
            var singletoninstance2 = (IOperationSingletonInstance)serviceProvider.GetService(typeof(IOperationSingletonInstance));


            return View(new OperationStatus
            {
                singleton1 = singleton1?.GetHashCode(),
                singleton2 = singleton2?.GetHashCode(),
                scoped1 = scoped1?.GetHashCode(),
                scoped2 = scoped2?.GetHashCode(),
                transient1 = transient1?.GetHashCode(),
                transient2 = transient2?.GetHashCode(),
                singletoninstance1 = singletoninstance1.GetHashCode(),
                singletoninstance2 = singletoninstance2.GetHashCode(),
                RootProvider = _serviceProvider.GetHashCode(),
                RequestProvider = _httpContextAccessor.HttpContext.RequestServices.GetHashCode(),
                singleton1Guid = singleton1.OperationId,
                singleton2Guid = singleton2.OperationId,
                scoped1Guid = scoped1.OperationId,
                scoped2Guid = scoped2.OperationId,
                transient1Guid = transient1.OperationId,
                transient2Guid = transient2.OperationId,
                singletoninstance1Guid = singletoninstance1.OperationId,
                singletoninstance2Guid = singletoninstance2.OperationId
            });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

    public class OperationStatus
    {
        public int? singleton1 { get; set; }
        public int? singleton2 { get; set; }
        public int? scoped1 { get; set; }
        public int? scoped2 { get; set; }
        public int? transient1 { get; set; }
        public int? transient2 { get; set; }
        public int? singletoninstance1 { get; set; }
        public int? singletoninstance2 { get; set; }
        public int? RootProvider { get; set; }
        public int? RequestProvider { get; set; }
        public Guid singleton1Guid { get; set; }
        public Guid singleton2Guid { get; set; }
        public Guid scoped1Guid { get; set; }
        public Guid scoped2Guid { get; set; }
        public Guid transient1Guid { get; set; }
        public Guid transient2Guid { get; set; }
        public Guid singletoninstance1Guid { get; set; }
        public Guid singletoninstance2Guid { get; set; }
    }
}
